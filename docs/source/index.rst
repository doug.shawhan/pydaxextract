.. PyDaxExtract documentation master file, created by
   sphinx-quickstart on Mon May 17 15:29:47 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PyDaxExtract
============

Extract DAX expressions from Power BI templates.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: dax_extract 
   :members:
   :private-members:

.. automodule:: scripts.daxextract
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
